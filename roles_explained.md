# Ansible Roles

The concept of an Ansible role is simple; it is a group of variables, tasks, files, and handlers that are stored in a standardized file structure.

The most complicated part of a role is recalling the directory structure, but there is help. The built-in ansible-galaxy command has a subcommand that will create our role skeleton for us.

Simply use ansible-galaxy init <ROLE_NAME> to create a new role in your present working directory. You will see here that several directories and files are created within the new role:

```

test
├── defaults
│   └── main.yml
├── files
├── handlers
│   └── main.yml
├── meta
│   └── main.yml
├── README.md
├── tasks
│   └── main.yml
├── templates
├── tests
│   ├── inventory
│   └── test.yml
└── vars
    └── main.yml

```
### Makeup of an Ansible Role

The list containing directories and files listed above are fairly straightforward. Most directories contain a main.yml file; Ansible uses each of those files as the entry point for reading the contents of the directory (except for files, templates, and test). You have the freedom to branch your tasks and variables into other files within each directory. But when you do this, you must use the include directive in a directory’s main.yml to have your files utilized as part of the role. We will take a closer look at this after a brief rundown of each directory’s purpose.

***defaults*** directory is designated for variable defaults that take the lowest precedence. Put another way: If a variable is defined nowhere else, the definition given in defaults/main.yml will be used.

The **files** and **templates** directories serve a similar purpose. They contain connected files and Ansible templates that are used within the role. The beautiful part about these directories is that Ansible does not need a path for resources stored in them when working in the role. Ansible checks them first. You may still use the full path if you want to reference files outside of the role, however, best practices suggest that you keep all of the role components together.

The **handlers** directory is used to store Ansible handlers. If you aren’t familiar, Ansible handlers are simply tasks that may be flagged during a play to run at the play’s completion. You may have as many or as few handlers as are needed for your role.

The **meta** directory contains authorship information, the person or team who wrote and is responsible for the role. The meta directory may also be used to define role dependencies. As you may suspect, a role dependency allows you to require that other roles be installed prior to the role in question.

The **README.md** file is simply a README file in markdown format. This file should include a general description of how your role operates, there's documentation available on the TPO website that goes into more detail.

The **task** directory is where most of your role will be written. This directory includes all the tasks that your role will run. Ideally, each logically related series of tasks would be laid out in their own files, and simply included through the main.yml file in the tasks directory.

The **test** directory contains a sample inventory and a test.yml playbook. This may be useful if you have an automated testing process built around your role.

The last directory created is the **vars** directory. This is where you create variable files that define necessary variables for your role. The variables defined in this directory are meant for role internal use only. It is a good idea to namespace your role variable names, to prevent potential naming conflicts with variables outside of your role. For example, if you needed a variable named trendmicro config or smurf log file in your baseline playbook, you may want to name your variable baseline_config_file, to avoid conflicts with another possible config_file variable defined elsewhere.

### Organizing Roles within a Version Control Systems

It's important that Roles are organized within a single version control repository to allow easy

- Access, download, clone
- Collaboration, download, clone, update using Pull request.
- Version control, link to a specific branch, tag or commit code. This will become very important when it comes time to link to Ansible Tower within Ansible Projects.

Contents of an Ansible Role in a VCS system

```
wordpress/
├── roles
│   └── wordpress
│       ├── defaults
│       │   └── main.yml
│       ├── files
│       ├── handlers
│       │   └── main.yml
│       ├── meta
│       │   └── main.yml
│       ├── molecule
│       │   └── default
│       │       ├── Dockerfile.j2
│       │       ├── INSTALL.rst
│       │       ├── molecule.yml
│       │       ├── playbook.yml
│       │       └── tests
│       │           ├── test_default.py
│       │           └── test_default.pyc
│       ├── README.md
│       ├── tasks
│       │   └── main.yml
│       ├── templates
│       ├── tests
│       │   ├── inventory
│       │   └── test.yml
│       └── vars
│           └── main.yml
└── wordpress.yml

```

executing the role above from the command line is as easy as executing the command below

``` ansible-playbook wordpress.yml -e "server_name=localhost" ```

and creating a project, job-template pointing to the same playbook

## Naming your playbook and Roles

What's in a name, having a name that clearly indicates what the playbook does is important.

Example of a good playbook name.
create_vm_snapshot.yml

**Example of a good role name**

```
vmware_snapshot/
├── defaults
│   └── main.yml
├── files
├── handlers
│   └── main.yml
├── meta
│   └── main.yml
├── README.md
├── tasks
│   └── main.yml
├── templates
├── tests
│   ├── inventory
│   └── test.yml
└── vars
    └── main.yml
```

Why is the above name a good name? it clearly identifies a few things.

- purpose, creating a snapshot.
- It identifies the technology being used to take the snapshot, in this case vm - Vmware. You could also even use create_vmware_snapshot.yml.
- When you creates Job templates within Ansible Tower, having a clearly defined name helps to identify the actions of the job template.