# Organizing Ansible Code within a Version Control system

Branches and Repositores, when should you use either or both?

## Repository

Definition of repository: a place, building, or receptacle where things are or may be stored.

The building in this case is a git repository which is usualy a directory. your local repository consists of three "trees" maintained by git.

- The first one is your Working Directory (.git), which holds the actual files. 
- The second one is the Index which acts as a staging area
- Third is the HEAD which points to the last commit you've made.

Repositories contain branch(s) and code reside within the branch's.

## Branch

Definitions of a branch: branches are effectively a pointer to a snapshot of your changes.

Branch's reside within a repository, sing the analogy above think of a branch as being a room within a building. Branches are used to develop code that are isolated from each other. The master branch is the "default" branch when you create a repository. You would Use other branches for development and merge (Promote) them to the master branch when they have been vetted (Code reviewed and signed off). In our case we are using three branch's

- Dev, used for mapping Ansible code to the Developmnet Tower and developing and testing out your code.
- UAT, used for mapping Ansible code to the UAT Tower, promoted from developement branch only.
- Master, used for mapping Ansible code to the Production tower, promoted from UAT branch only.

### Reason for using Repositories and branchs

**Repositories** As indicated branch's reside in repositories as that is how your code is organized. If you wanted to change ownershiop or allow someone to copy (clone) your code. Whether your using Bitbucket or GitHub both tools allow you to grant permission to individuals or teams to allow or deny access to the code.

If you were creating two projects,

- project Snapshot
- project Gravity

You are developeing code for both projects and you need to hand both projects over to two different teams, It's much easier to develope each project in seperate repositories, reason.

- It allows the code to retain all history and reasonning within the code.
- The repository contains code only relevant to that project, clear seperation.
- Allows someone to clone only the code relevant to that project.

**Branch** Branch's store related code there's no way to seperate the projects mentioned above clearly and for the same reason's mentioned abover.

