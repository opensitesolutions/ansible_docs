# Ansible

Ansible was written about six years ago by Michael Dehaan, https://michaeldehaan.net/, https://github.com/mpdehaan. It’s name is derived on a science fiction reference. An "ansible" is a fictional communication device that can transfer information faster than the speed of light. The author Ursula K. Le Guin invented the concept in her book Rocannon’s World, and other sci-fi authors have since borrowed the idea from Le Guin. More specifically, Michael DeHaan took the name Ansible from the book Ender’s Game by Orson Scott Card. In that book, ansible was used to control a large number of remote ships at once, over vast distances.

I have been personally using Ansible since version 1.9

## differences between ansible and other tools similiar

Ansible is a tool written using python leveraging well known Unix applications

- ssh
- sshpass
- expect
- shells

All via Python modules.

Ansible requires no agents, originally all ansible needed was ssh, and python installed on the remote server.
Now ansible can work with windows making use of Windows powershell.

## Configuration Method

Unlike tools such as Puppet, Chef, ansible is a push based system leveraging

- SSH for gainig systems access.
- PKI for authentication
- SUDO for Authorization

Ansible then copies python modules to the systems and executes task outlined within the playbook based on the module identified within the playbook.

## What is Ansible

IT automation, configration managment and provisioing tool, uses playbooks to build, manage, test and configure anything  from full server environment to web sites It's a tools that replaces manual steps. that was manualy done
-configuration management, cloud automation, deployment, orchestration

## Documentation Links

- https://docs.ansible.com/ - Ansible documentation
- https://ansible-manual.readthedocs.io/en/stable-1.9/ -Ansible docs READ THE docs
- https://docs.ansible.com/ansible/devel/roadmap/ - Road Map
- https://docs.ansible.com/ansible/latest/community/ - community Guide
- https://github.com/ansible/ansible - Ansible project

## Ansible videos

- How ansible works: https://www.youtube.com/watch?v=St__HLMZ8qQ
- introduction to Ansible Playbooks: https://www.youtube.com/watch?v=ZAdJ7CdN7DY
- How to Manage Multiple Operating System Types with a Single Ansible Inventory: https://www.youtube.com/watch?v=5gLG5N7tzmU
- Ansible essentials: https://www.ansible.com/resources/webinars-training/introduction-to-ansible

- Ansible 2.0 and beyond: https://www.ansible.com/resources/webinars-training/ansible-2.0-beyond
    - Task blocks
    - Execution strategies
    - Refined playbook parser
    - Increased flexibility
    - New modules and plugin
