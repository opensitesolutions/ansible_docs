# Code review

- What is code review
- How to do a code review
- Benefits of a code review

## What is a code review

Code review process is a phase in creating code where the authors of the code, peer reviewer(s) get together to review the code. Code review is essentially what is referred as Pull request in the software field. Pull request are fundamental to how teams propose new features, opportunities for changing and enhancing not just code but processes within your team and importantly reviewing and improving the cod.
 
 - how to implement change.
 - how to verify changes.
 - Plan for failure.
 - Create a plan for when thinks fail.


## How to do a code review

A code review is the process of merging different branch's of code together. Usually the merging of code is between some branch to the master branch. Code review is initiated by a Pull Request that set in motion the following process, the process being.

1. Developers creates workflow branch

2. Developer writes code within the workflow branch and regularly checks the code in both local and to the remote branch.

3. Developers has finished writing and testing the code and wants to merge code into the master branch.

4. Pull request is initiated from developers source workflow branch to destination dev branch.

3. Peer review is initiated.


In our case we are merging code from your working branch to Development branch

clarence_vm_snaphshot_role  ===> dev 

### Actions

### Code submission

- A clear useful description of the changes and give a general overview of why the change is necessary.
- The scope of the change
- Area's where the reviewers may want to give special attention.
- Details that may help reviewers better understand the patch.
- Test results.

### Reviewer

- Is the code concise and straightforward.
- Does the documentation match what the role or playbook indicates.
- Test the code yourself or with the coder to confirm the playbook or role completes without any errors.
- Look for any coding issues, recommend coding alternative, best practices.

### Results

The end result should result in the code being approved, you will need to capture the following information.

- The last commit number (chronological order).
- Successful job runs of the code being executed from Tower.

## Benefits of code review

### Current process

- We have two branches development and master branch to which everyone commits.
- Developers work off the development and master branch and push test and changes directly to the branch.
- Developers request code review on the last commit.

### ProblemS

- Any bugs that in the code review are already in master or development branch by the time it's caught.
- Code in the development and master branch would cause someone a few hours trying to figure out what happened after code has been deployed within the environments.